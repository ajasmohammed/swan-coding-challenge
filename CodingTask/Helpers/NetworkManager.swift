//
//  NetworkManager.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import Alamofire
import ObjectMapper
import Foundation
import CommonCrypto

class NetworkManager{
    private static var alamoFireManager: Session? = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 60
        configuration.timeoutIntervalForResource = 60
      let alamoFireManager = Alamofire.Session(configuration: configuration)
        return alamoFireManager

    }()
    
    public static func getWeatherDataCityID(cityID : Int,completionHandler: @escaping (Any) -> Void, errorHandler: @escaping (String,String) -> Void){
        
        let urlString = "http://api.openweathermap.org/data/2.5/forecast?id=\(cityID)&mode=json&units=metric&appid=\(ServiceUtils.apiKey)"
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }

    
    public static func getWeatherDatabyCityName(cityName : String,completionHandler: @escaping (Any) -> Void, errorHandler: @escaping (String,String) -> Void){
        
        
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(cityName)&mode=json&units=metric&appid=\(ServiceUtils.apiKey)"
        request(urlStr: urlString, completionHandler: completionHandler, errorHandler: errorHandler)
    }

    
    //Request processing methoda
    
    
    
    private static func request(urlStr: String, method: HTTPMethod = .get, parameters: Parameters = [:],myEncoding: ParameterEncoding = URLEncoding.default,completionHandler: @escaping (Any) -> Void,headers: HTTPHeaders? = nil, errorHandler: @escaping (String,String) -> Void) {
        if ServiceUtils.isConnectedToNetwork() {
            
            let dataRequest = alamoFireManager?.request(urlStr, method: method, parameters: parameters, encoding: myEncoding, headers: headers)
            
            
            
            dataRequest?.validate().responseJSON { response in
                let errorCodeString = String(response.response?.statusCode ?? -1)
                
                switch (response.result) {
                case .success:
                    processResponse(response: response, completionHandler: completionHandler, errorHandler: errorHandler)
                    break
                case .failure(_):
                    
                    errorHandler(errorCodeString,ServiceUtils.requestError)
                    break
                }
            }
            
        } else {
            
            errorHandler("-1",ServiceUtils.genericError)
        }
    }
    
    
    
  
    
    
    private static func processResponse(response: AFDataResponse<Any>, completionHandler: @escaping (Any) -> Void, errorHandler: @escaping (String,String) -> Void) {
        if ServiceUtils.DEBUG_MODE {
            print("Request: \(String(describing: response.request))")
            print("Response: \(String(describing: response.response))")
        }
        
        let errorCodeString = String(response.response?.statusCode ?? -1)
        
        
        switch response.result {
        case let .success(JSON):
            
            completionHandler(JSON)
//            if let requestResult = Mapper<WeatherModel>().map(JSONObject: JSON) {
//
//            }
//            else {
//                errorHandler(errorCodeString,"Sorry, we are facing an error for processing this request. Please try again later.")
//            }
         
        case let .failure(error):
            var errorString = error.localizedDescription
            if let data = response.data, let responseDataStr = String(data: data, encoding: String.Encoding.utf8) {
                errorString = responseDataStr
            }
            errorHandler(errorCodeString,errorString)
        }
        
        
    }
    
    
    
  

    
    
}

struct JSONStringArrayEncoding: ParameterEncoding {
    private let myString: String

    init(string: String) {
        self.myString = string
    }

    func encode(_ urlRequest: URLRequestConvertible, with parameters: Parameters?) throws -> URLRequest {
        var urlRequest = try urlRequest.asURLRequest()

        let data = Data(myString.utf8)

        if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        urlRequest.httpBody = data

        return urlRequest
    }
}


