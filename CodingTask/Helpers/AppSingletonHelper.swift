//
//  AppSingletonHelper.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import Foundation
import ObjectMapper

class AppSingletonHelper: NSObject {
    
    static let shared = AppSingletonHelper()
    var cityData :[CityModel]?
    var currentCityCode = ""
    
    
    private override init() {
    //SingleTon class
    }
    
    func getAllCityData(fileName : String,completionHandler: @escaping ([CityModel]) -> Void, errorHandler: @escaping (String,String) -> Void){
        
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                   if let model = CityModel.toObjectArray(json: jsonResult)
                   { completionHandler(model)
                   }
                else {
                    errorHandler("-999",ServiceUtils.genericError)
                }
            } catch let error {
                errorHandler("-999",error.localizedDescription)
            }
        }
    }

}
