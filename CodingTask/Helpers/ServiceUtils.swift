//
//  ServiceUtils.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import Foundation

class ServiceUtils {
    
    
    public static let apiKey =  "6b30e0efe72009e71ca383fe1cc28a3b"

    public static let DEBUG_MODE = false
    
    public static let genericError = "Sorry, no internet connection detected. Please reconnect and try again"
    public static let requestError = "Sorry, We are not able process this request"
    public static let cityError = "Sorry, We are not able process this get weather for this city"
    public static let locationError = "Location service is disabled!!"
    public static var NotificaionLocationTracked = "LocationTracked"
    public static let genericErrorCode = "-1"
    public static let EmptySearchBarError = "Kindly enter the city name to get city's current weather"
    
    

  
    
    /*****************************        Method Names        ********************************/
    
    class func isConnectedToNetwork() -> Bool {
        return Reachabilitycustom.isConnectedToNetwork()
    }
    
    
    //****************************** CONSTANTS ****************************************
    
    
    public static let Network_timeout = 60.0
}
