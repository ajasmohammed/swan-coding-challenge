//
//  LocationHelper.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 18/06/2021.
//

import Foundation
import CoreLocation
extension AppDelegate : CLLocationManagerDelegate{
    
    func registerLocation(){
        locationManager.requestWhenInUseAuthorization()

        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        locationManager.startMonitoringSignificantLocationChanges()
       updateLocationPermission()
        // Here you can check whether you have allowed the permission or not.
        
    }
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
            case .authorizedAlways, .authorizedWhenInUse:
                updateLocationPermission()
            default:
                break
            }
        }
    }
    func updateLocationPermission(){
        if CLLocationManager.locationServicesEnabled()
        {
            switch(CLLocationManager.authorizationStatus())
            {
            case .authorizedAlways, .authorizedWhenInUse:
                if !self.islocationCalled{
                    self.islocationCalled = true
                    let latitude: CLLocationDegrees = (locationManager.location?.coordinate.latitude)!
                    let longitude: CLLocationDegrees = (locationManager.location?.coordinate.longitude)!
                    let location = CLLocation(latitude: latitude, longitude: longitude) //changed!!!
                    CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
                        if error != nil {
                            return
                        }else if let cityName = placemarks?.first?.locality
                        {
                            AppSingletonHelper.shared.currentCityCode = cityName
                            self.islocationCalled = false
                            NotificationCenter.default.post(name: Notification.Name(ServiceUtils.NotificaionLocationTracked), object: nil)
                            
                        }
                        
                    })
                }
               
                break
                
            case .notDetermined:
                print("Not determined.")
                break
                
            case .restricted:
                print("Restricted.")
                break
                
            case .denied:
                print("Denied.")
            @unknown default:
                print("unknown.")
            }
        }
        

    }
   
}
