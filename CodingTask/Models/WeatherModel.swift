//
//  WeatherModel.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//
import Foundation
import ObjectMapper

struct WeatherModel : Mappable {
	var cod : String?
	var message : Int?
	var cnt : Int?
	var list : [List]?
	var city : City?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		cod <- map["cod"]
		message <- map["message"]
		cnt <- map["cnt"]
		list <- map["list"]
		city <- map["city"]
	}
    
    static func toObject(json : Any?) -> WeatherModel?{
        return Mapper<WeatherModel>().map(JSONObject:json)
    }

}
