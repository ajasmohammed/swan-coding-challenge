//
//  CurrentWeatherModel.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 18/06/2021.
//

import ObjectMapper
struct CurrentWeatherModel : Mappable {
    var cod : Int?
    var name : String?
    var id : Int?
    var time : Int?
    var weather : [Weather]?
    var main : Main?
    var wind : Wind?
    var visibility : Int?

    init?(map: Map) {

    }

    mutating func mapping(map: Map) {

        cod <- map["cod"]
        name <- map["name"]
        id <- map["id"]
        time <- map["time"]
        weather <- map["weather"]
        main <- map["main"]
        wind <- map["wind"]
        visibility <- map["visibility"]
    }
    
    static func toObject(json : Any?) -> CurrentWeatherModel?{
        return Mapper<CurrentWeatherModel>().map(JSONObject:json)
    }

}
