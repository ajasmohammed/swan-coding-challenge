//
//  Weather.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//
import Foundation
import ObjectMapper

struct Weather : Mappable {
	var id : Int?
	var main : String?
	var weatherDescription : String?
	var icon : String?

	init?(map: Map) {

	}

	mutating func mapping(map: Map) {

		id <- map["id"]
		main <- map["main"]
        weatherDescription <- map["description"]
		icon <- map["icon"]
	}

}
