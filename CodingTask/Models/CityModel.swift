//
//  CityModel.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import Foundation
import ObjectMapper

struct CityModel : Mappable {
    
    var id : Int?
    var name : String?
    var state : String?
    var country : String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        name <- map["name"]
        state <- map["state"]
        country <- map["country"]
    }
    
    static func toObjectArray(json : Any) -> [CityModel]?{
        return Mapper<CityModel>().mapArray(JSONObject:json)
    }
     
    
    
}

