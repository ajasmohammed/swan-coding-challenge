//
//  ViewController.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import UIKit
import CoreLocation
class DashBoardVC: BaseVC {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var permissionView: UIView!
    private var weatherModel : WeatherModel?

  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Current City"
        permissionView.isHidden = false
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.locationObserver(notification:)), name: Notification.Name(ServiceUtils.NotificaionLocationTracked), object: nil)
        let cityName = AppSingletonHelper.shared.currentCityCode
        if cityName != ""{
            updateLocationDetails()
        }
        
    }
    
    @objc func locationObserver(notification: Notification) {
        updateLocationDetails()
    }

    @IBAction func goToSettting(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)

    }
    
    func updateLocationDetails(){
      
        let cityName = AppSingletonHelper.shared.currentCityCode
        if cityName != ""{
            self.showProgressHUD()
            permissionView.isHidden = true
            self.getCityCodeByLocalCityData(city: cityName)
        }else{
            permissionView.isHidden = false
            self.showErrorHandlerAlert(errcode:ServiceUtils.genericErrorCode,errMsg: ServiceUtils.locationError)
        }
        tableView.dataSource = self
    }
    
    func getCityCodeByLocalCityData(city : String){
        self.title = city
        if let citydata = AppSingletonHelper.shared.cityData{
            if let cityFound =  citydata.filter({ item in
                item.name == city
            }).first{
                self.getFiveDaysWeatherData(cityID: cityFound.id!)
            }else{
                self.showErrorHandlerAlert(errcode:ServiceUtils.genericErrorCode,errMsg: ServiceUtils.cityError)
                
            }
        }
    }
    
    func getFiveDaysWeatherData(cityID :Int ){
       // 292223
        
        NetworkManager.getWeatherDataCityID(cityID:cityID,completionHandler: { response in
            if let value = response as? [String : Any]{
                if let model = WeatherModel.init(JSON: value){
                    self.hideProgressHUD()
                    self.weatherModel = model
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                    
                }else{
                    self.showErrorHandlerAlert(errcode:ServiceUtils.genericErrorCode,errMsg: ServiceUtils.genericError)
                }
            }
        } ,errorHandler:showErrorHandlerAlert)
    }

}

extension DashBoardVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WeatherTableCell.className, for: indexPath) as! WeatherTableCell
        
        guard let weatherItem = weatherModel?.list?[indexPath.row] else {
            return UITableViewCell()
        }
        
        cell.minMaxLabel.text = "\(String(describing: weatherItem.main!.temp_min!)) / \(String(describing: weatherItem.main!.temp_max!)) °C"
        cell.weatherDescLabel.text = "\(String(describing: weatherItem.weather!.first!.weatherDescription!))"
        cell.windSpeedLabel.text = "\(String(describing: weatherItem.wind!.speed!)) m\\s"
        cell.weatherDateLabel.text = dateFormater(date: weatherItem.dt_txt!)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        weatherModel?.list?.count ?? 0
    }
    
   
}
    




