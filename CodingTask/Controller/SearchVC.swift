//
//  SearchVC.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 18/06/2021.
//

import UIKit
import RealmSwift
class SearchVC: BaseVC {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var recentSearchView: UIView!
    @IBOutlet weak var searchResultView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    var recentSearch = [String]()
    var currentWeatherModel : CurrentWeatherModel?
    private let noSearchItemMessgae = "No recent search found"
    override func viewDidLoad() {
        super.viewDidLoad()
        getRecentSearchLocally()
        searchResultView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        // Do any additional setup after loading the view.
    }

    @IBAction func doSearch(_ sender: UIButton) {
        if let searchkey = searchBar.text,!searchkey.isEmpty{
            getCurrentCityWeather(cityName: searchkey)
            
        }else{
            self.showErrorHandlerAlert(errcode: ServiceUtils.genericErrorCode, errMsg: ServiceUtils.EmptySearchBarError )
        }
    }
    
    func getCurrentCityWeather(cityName : String){
        self.showProgressHUD()
        NetworkManager.getWeatherDatabyCityName(cityName: cityName, completionHandler: { response in
            self.hideProgressHUD()
            if let value = response as? [String : Any]{
                if let model = CurrentWeatherModel.init(JSON: value){
                    self.storeRecentSearchLocally(name:cityName)
                    self.currentWeatherModel = model
                    DispatchQueue.main.async {
                        self.bindUI()
                    }
                    
                }else{
                    self.showErrorHandlerAlert(errcode:ServiceUtils.genericErrorCode,errMsg: ServiceUtils.cityError)
                }
            }
        }) { code, errMsg in
            self.showErrorHandlerAlert(errcode: code, errMsg:  ServiceUtils.cityError)
        }
    }
    
    
    private func bindUI(){
        
        if let model = currentWeatherModel{
            temperatureLabel.text = "\(String(describing: model.main!.temp_min!)) / \(String(describing: model.main!.temp_max!)) °C"
           weatherDescriptionLabel.text = "\(String(describing: model.weather!.first!.weatherDescription!))"
            windSpeedLabel.text = "\(String(describing: model.wind!.speed!)) m\\s"
            cityNameLabel.text = model.name
            recentSearchView.isHidden = true
            searchResultView.isHidden = false

        }
        
        
    }
}

//Managing TableView and SearchBar
extension SearchVC : UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentSearch.count == 0 ? 1 : recentSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableCell.className, for: indexPath) as! SearchTableCell
        if recentSearch.count != 0{
            cell.searchLabel.text = recentSearch[indexPath.row]
            if indexPath.row + 1 == recentSearch.count {
                cell.separatorView.isHidden = true
            }else{
                cell.separatorView.isHidden = false
            }
        }else{
            cell.searchLabel.text = noSearchItemMessgae
            cell.separatorView.isHidden = true
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if recentSearch.count != 0{
            searchBar.text =  recentSearch[indexPath.row]
            recentSearch.remove(at: indexPath.row)
            getCurrentCityWeather(cityName: searchBar.text!)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchkey = searchBar.text,!searchkey.isEmpty{
        }
        else{
            getRecentSearchLocally()
            searchResultView.isHidden = true
            recentSearchView.isHidden = false
            tableView.reloadData()
        }
    }
    

    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        if let searchkey = searchBar.text,!searchkey.isEmpty{
            getCurrentCityWeather(cityName: searchkey)
            
        }else{
            self.showErrorHandlerAlert(errcode: ServiceUtils.genericErrorCode, errMsg: ServiceUtils.EmptySearchBarError )
        }
    }
   
}


//Managing the local Realm DB

extension SearchVC{
    
    
    
    private func getRecentSearchLocally(){
        
        do{
            let searchResult = try Realm().objects(RecentSearchObjects.self).sorted(byKeyPath: "id", ascending: false)
            recentSearch = [String]()
            //limiting recent 5 search city
            var i = 0
            for item in searchResult{
                if i<5{recentSearch.append(item.name)}
                else{break}
                i+=1
            }
        }catch{
            print("Realm object failed while reading")
        }
        
        tableView.reloadData()
    }
    
    func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(RecentSearchObjects.self).max(ofProperty: "id") as Int? ?? 0) + 1
    }
    
    private func storeRecentSearchLocally(name:String){
        
        do{
            let realm = try Realm()
            //delet the object if already present locally
            
            do{
                if let alreadyPresent =  realm.objects(RecentSearchObjects.self).filter("name == %@",name).first
                {
                    try realm.write {
                        realm.delete(alreadyPresent)
                    }
                    
                }
            }catch{ print("Realm operation failed while reading already present data")}
            try realm.write {
                let  recent = RecentSearchObjects()
                recent.name = name
                recent.id = incrementID()
                realm.add(recent)
            }
        }catch{
            print("Realm operation failed while storing")
        }
        
    }
}
