//
//  BaseVC.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import UIKit
class BaseVC : UIViewController{
    var  progressHUD : ProgressHUD?

    
    func hideProgressHUD() {
        if let progressHUD = self.progressHUD {
            self.view.isUserInteractionEnabled = true
            progressHUD.removeFromSuperview()
            self.progressHUD = nil
        }
    }
    
    func showProgressHUD(text: String = "Loading...") {
            self.view.isUserInteractionEnabled = false
                    progressHUD = ProgressHUD(text: text)
                    self.view.addSubview(progressHUD!)
        }
    
    func dateFormater(date : String) -> String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "E, d MMM, h:mm a"
        
        if let date = dateFormatterGet.date(from: date) {
            return(dateFormatterPrint.string(from: date))
        } else {
            return date
        }

    }
    
    func showErrorHandlerAlert(errcode:String,errMsg:String){
        self.hideProgressHUD()
        showErrorMessage(message: errMsg, viewController: self, handler: nil)
    }
    
    private func showErrorMessage(message: String, viewController: UIViewController, handler: ((UIAlertAction) -> Void)?) {
        let refreshAlert = UIAlertController(title: nil, message: message, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Okay", style: .default, handler: handler))
        
        viewController.present(refreshAlert, animated: true, completion: nil)
    }
    
}
