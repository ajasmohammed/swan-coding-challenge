//
//  AppDelegate.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 17/06/2021.
//

import UIKit
import CoreLocation
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  var locationManager = CLLocationManager()
    var islocationCalled = false

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        AppSingletonHelper.shared.getAllCityData(fileName: "cityList", completionHandler: { model in
            AppSingletonHelper.shared.cityData = model
        }) { code, errorMsg in
            print(errorMsg)
        }
        registerLocation()
        return true
    }

}

