//
//  WeatherTableCell.swift
//  CodingTask
//
//  Created by Ajas Mohammed on 18/06/2021.
//

import UIKit
class WeatherTableCell: UITableViewCell{
    
    @IBOutlet weak var weatherDateLabel: UILabel!
    @IBOutlet weak var minMaxLabel: UILabel!
    @IBOutlet weak var weatherDescLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
}
